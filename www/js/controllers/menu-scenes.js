controllers.controller('SceneMenuCtrl', function ($scope, SceneService) {
	var scenemenu = this;

	// set events
	$scope.$on('$ionicView.enter', function (e) {
		refresh();
	});
	$scope.$on('scene.refresh', function () {
		refresh();
	});
	$scope.clickscene = function (index) {
		sceneService.switchon(index).then(function (res) {
		}, function (res) {
		});
	};
	// internal functions
	var refresh = function () {
		SceneService.init().then(function () {
			scenemenu.scenes = SceneService.list;
			console.log(scenemenu.scenes);
		}, function (error) {
		});
	};
	setTimeout(function () {
		refresh();
	}, 2000);
});