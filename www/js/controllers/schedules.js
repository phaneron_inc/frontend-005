controllers.controller('SchedulesCtrl', function($scope, ScheduleService, FormatService, $timeout, ionicMaterialMotion, ionicMaterialInk) {
	// definitions and initializations
	var schedulesboard = this;
	var animateflag = false;
	schedulesboard.schedules = [];

	// events
	$scope.$on('tasks', function (event) {
		refresh(animateflag);
	});
	$scope.$on('task', function (event, id) {
		console.log('on.task called!');
		refresh(animateflag, id);
	});
	$scope.$on('$ionicView.enter',function (e) {
		console.log('$ionicView.enter() called!');
		refresh(animateflag);
	});
	$scope.$on('ngLastRepeat.tasks', function (event) {
		animateflag = true;
		animate();
	});
	$scope.refreshpage = function () {
		refresh(false);
	};
	var refresh = function (flag, id) {
		console.log(schedulesboard.schedules)
		$scope.$apply(function () {
			if (typeof id === 'undefined') {
				console.log(flag, id);
				schedulesboard.schedules = ScheduleService.getTasks();
				console.log(schedulesboard.schedules);
				if (flag===true) animate();
				else { $scope.$broadcast('scroll.refreshComplete'); }
			}
			if (typeof id !== 'undefined') {
				_.map(schedulesboard.schedules, function (schedule) {
					if (schedule.id == id) return ScheduleService.getTask(id);
					else return schedule;
				});
				$scope.$broadcast('scroll.refreshComplete');
			}
		});
	}	
	
	var animate = function () {
		$scope.$parent.showHeader();
		$scope.$parent.clearFabs();
		$scope.isExpanded = true;
		$scope.$parent.setExpanded(true);
		$scope.$parent.setHeaderFab('right');
		// Activate ink for controller
		ionicMaterialInk.displayEffect();
		$timeout(function () {
			ionicMaterialMotion.fadeSlideIn({
				selector: '.animate-blinds .item'
			});
			$scope.$broadcast('scroll.refreshComplete');
		}, 200);
	};
});