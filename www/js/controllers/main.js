controllers.controller('MainCtrl', function ($scope, $ionicModal, $ionicPopover, $timeout) {
	$scope.isExpanded = true;
	$scope.hasHeaderFabLeft = false;
	$scope.hasHeaderFabRight = false;
	$scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
		var to_state = 'to.'+toState.name.replace(/tab\./g, "");
		var from_state = 'from.'+fromState.name.replace(/tab\./g, "");
		$scope.$broadcast(to_state);
		$scope.$broadcast(from_state);
	});
	$scope.isExpanded = false;
	$scope.hasHeaderFabLeft = false;
	$scope.hasHeaderFabRight = false;
	var navIcons = document.getElementsByClassName('ion-navicon');
	for (var i = 0; i < navIcons.length; i++) {
		navIcons.addEventListener('click', function () {
			this.classList.toggle('active');
		});
	};

	////////////////////////////////////////
	// Layout Methods
	////////////////////////////////////////
	$scope.hideNavBar = function () {
		document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
	};
	$scope.showNavBar = function () {
		document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
	};
	$scope.noHeader = function () {
		var content = document.getElementsByTagName('ion-content');
		for (var i = 0; i < content.length; i++) {
			if (content[i].classList.contains('has-header')) {
				content[i].classList.toggle('has-header');
			}
		}
	};
	$scope.setExpanded = function (bool) {
		$scope.isExpanded = bool;
	};
	$scope.setHeaderFab = function (location) {
		var hasHeaderFabLeft = false;
		var hasHeaderFabRight = false;

		switch (location) {
			case 'left':
				hasHeaderFabLeft = true;
				break;
			case 'right':
				hasHeaderFabRight = true;
				break;
		}

		$scope.hasHeaderFabLeft = hasHeaderFabLeft;
		$scope.hasHeaderFabRight = hasHeaderFabRight;
	};
	$scope.hasHeader = function () {
		var content = document.getElementsByTagName('ion-content');
		for (var i = 0; i < content.length; i++) {
			if (!content[i].classList.contains('has-header')) {
				content[i].classList.toggle('has-header');
			}
		}

	};
	$scope.hideHeader = function () {
		$scope.hideNavBar();
		$scope.noHeader();
	};
	$scope.showHeader = function () {
		$scope.showNavBar();
		$scope.hasHeader();
	};
	$scope.clearFabs = function () {
		var fabs = document.getElementsByClassName('button-fab');
		if (fabs.length && fabs.length > 1) {
			fabs[0].remove();
		}
	};
});