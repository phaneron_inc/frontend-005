controllers.controller('LightCtrl', function($scope, $stateParams, LightService, ScheduleService, FormatService, $timeout, ionicMaterialMotion, ionicMaterialInk, ionicTimePicker) {
	// One time animation
	// Set Header
	$scope.$parent.showHeader();
	$scope.$parent.clearFabs();
	$scope.isExpanded = false;
	$scope.$parent.setExpanded(true);
	$scope.$parent.setHeaderFab('right');
	// Activate ink for controller
	ionicMaterialInk.displayEffect();
	var lightboard = this;
	var animateflag = false;
	lightboard.edited = false;
	lightboard.light = {};
	$scope.$on('$ionicView.enter',function (e) {
		refresh(animateflag);
		animateflag = true;
	});
	// $scope.$on('lights', function (event) {
	// 	refresh(animateflag);
	// });
	$scope.$on('light', function (event, id) {
		refresh(animateflag, id);
	});
	$scope.$on('lights-toggle', function (event, id) {
		refresh(false);
	});
	$scope.updateTask = function (newid, oldid) {
		console.log(newid, oldid);
		console.log(typeof newid);
		console.log(typeof oldid);
		if (ScheduleService.isValidTask(oldid)) {
			LightService.unsetTask(lightboard.light.id, oldid).then(function (res) {
				LightService.setTask(lightboard.light.id, newid).then(function (taskid) {
					$scope.$apply(function () {
						lightboard.task = ScheduleService.getTask(taskid);
						console.log(lightboard.task);
					});
					// refresh(false);
				}, function (err) {
					console.log(err);
				})
			}, function (err) {
				console.log(err);
			});
		} else {
			LightService.setTask(lightboard.light.id, newid).then(function (taskid) {
				$scope.$apply(function () {
					lightboard.task = ScheduleService.getTask(taskid);
					console.log(lightboard.task);
				});
				// refresh(false);
			}, function (err) {
				console.log(err);
			})
		}
	};
	$scope.refreshpage = function () {
		refresh(false);
	};
	var refresh = function (flag, id) {
		var lightid = id || $stateParams.id;
		$scope.$apply(function () {
			lightboard.light = LightService.getLight(lightid);
			var taskid = lightboard.light.taskid;
			if (taskid != null && typeof taskid !== 'undefined') {
				lightboard.task = ScheduleService.getTask(taskid);
			} else lightboard.task = {id:null};
			lightboard.tasks = ScheduleService.getTasks();
			if (flag===true) animate();
			else { $scope.$broadcast('scroll.refreshComplete'); }
		});
	};
	var animate = function () {
		$scope.$parent.showHeader();
		$scope.$parent.clearFabs();
		$scope.isExpanded = true;
		$scope.$parent.setExpanded(true);
		$scope.$parent.setHeaderFab('right');
		// Activate ink for controller
		ionicMaterialInk.displayEffect();
		$timeout(function () {
			ionicMaterialMotion.fadeSlideIn({
				selector: '.animate-blinds .item'
			});
			$scope.$broadcast('scroll.refreshComplete');
		}, 200);
	};
});
