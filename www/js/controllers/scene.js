String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
controllers.controller('ScenesBrd', function ($scope, $rootScope, $state, $ionicLoading, ionicMaterialMotion, ionicMaterialInk, SceneService) {
	var scenesboard = this;
	var del_light_flag = false;
	var del_appln_flag = false;
	// Set Header
	$scope.$parent.showHeader();
	$scope.$parent.clearFabs();
	$scope.isExpanded = false;
	$scope.$parent.setExpanded(true);
	$scope.$parent.setHeaderFab(false);
	// Set Ink
	ionicMaterialInk.displayEffect();
	// init data
	SceneService.init().then(function () {
		scenesboard.scenes = SceneService.list;
		$scope.$on('ngLastRepeat.scenes',function (e) {
			ionicMaterialMotion.fadeSlideIn();
		});
	}, function (error) {
	});
	$scope.$on('scene.add', function () {
		$scope.toggle_add_scene();
	});
	$scope.$on('$ionicView.enter', function (e) {
		refresh();
	});
	$scope.refresh_sceneboard = function () {
		refresh();
	};
	// scene functions
	$scope.toggle_add_scene = function (param) {
		if(scenesboard.add_scene) delete scenesboard.add_scene;
		else scenesboard.add_scene = true;
		if (typeof param === 'boolean') {
			if (param) {
				SceneService.create(scenesboard.new_scene_name).then(function (res) {
					delete scenesboard.new_scene_name;
					scenesboard.scenes = SceneService.all;
					$rootScope.$broadcast('scene.refresh');
					$state.go('tab.scene',{ index: SceneService.all.length - 1 });
				}, function (res) {
				});
			} else {
				delete scenesboard.add_scene;
			}
		};
	};
	var refresh = function () {
		SceneService.init().then(function () {
			scenesboard.scenes = SceneService.list;
		}, function (error) {
			// TO DO: error handling
		}).finally(function () {
			$scope.$broadcast('scroll.refreshComplete');
		});
	};
});

controllers.controller('ScenePage', function ($scope, $rootScope, $stateParams, $ionicHistory, $ionicTabsDelegate, $ionicScrollDelegate, $ionicLoading, ionicMaterialMotion, ionicMaterialInk, SceneService) {
	var scenepage = this;
	// codes
	scenepage.tvcodes = [
		{command: 'Power'		,code: '0xE0E040BF'},
		{command: 'Volume Up'	,code: '0xE0E0E01F'},
		{command: 'Volume Dowm'	,code: '0xE0E0D02F'},
		{command: 'Mute'		,code: '0xE0E0F00F'}
	];
	scenepage.accodes = [
		{command: 'On'			,code: '0x880074B'},
		{command: 'Off'			,code: '0x88C0051'},
		{command: 'Temp 16°C'	,code: '0x880814D'},
		{command: 'Temp 17°C'	,code: '0x880824E'},
		{command: 'Temp 18°C'	,code: '0x880834F'},
		{command: 'Temp 19°C'	,code: '0x8808440'},
		{command: 'Temp 20°C'	,code: '0x8808541'},
		{command: 'Temp 21°C'	,code: '0x8808642'},
		{command: 'Temp 22°C'	,code: '0x8808743'},
		{command: 'Temp 23°C'	,code: '0x8808844'},
		{command: 'Temp 24°C'	,code: '0x8808945'},
		{command: 'Temp 25°C'	,code: '0x8808A46'},
		{command: 'Temp 26°C'	,code: '0x8808B47'},
		{command: 'Temp 27°C'	,code: '0x8808C48'},
		{command: 'Temp 28°C'	,code: '0x8808D49'},
		{command: 'Temp 29°C'	,code: '0x8808E4A'},
		{command: 'Temp 30°C'	,code: '0x8808F4B'},
		{command: 'Fan Low'		,code: '0x8808F07'},
		{command: 'Fan Med'		,code: '0x8808F29'},
		{command: 'Fan High'	,code: '0x8808F4B'}
	];
	scenepage.curtaincodes = [
		{command: 'Open'		,code: '2'},
		{command: 'Close'		,code: '0'},
		{command: 'Pause'		,code: '1'}
	];
	scenepage.rgbcodes = [
		{command: 'On'			, code: '0xF7C03F'},
		{command: 'Off'			, code: '0xF740BF'},
		{command: 'Bright +'	, code: '0xF700FF'},
		{command: 'Bright -'	, code: '0xF7807F'},
		{command: 'Flash'		, code: '0xF7D02F'},
		{command: 'Strobe'		, code: '0xF7F00F'},
		{command: 'Fade'		, code: '0xF7C837'},
		{command: 'Smooth'		, code: '0xF7E817'},
		{command: 'White'		, code: '0xF7E01F'},
		{command: 'Red'			, code: '0xF720DF'},
		{command: 'Green'		, code: '0xF7A05F'},
		{command: 'Blue'		, code: '0xF7609F'},
		{command: 'Cyan'		, code: '0xF7B04F'},
		{command: 'Magenta'		, code: '0xF76897'},
		{command: 'Yellow'		, code: '0xF728D7'},
		{command: 'Orange'		, code: '0xF710EF'},
		{command: 'Gold'		, code: '0xF708F7'},
		{command: 'Purple'		, code: '0xF7708F'},
		{command: 'Indigo'		, code: '0xF748B7'},
		{command: 'Violet'		, code: '0xF750AF'},
		{command: 'Coral'		, code: '0xF730CF'},
		{command: 'Chartreuse'	, code: '0xF7906F'},
		{command: 'Darkcyan'	, code: '0xF78877'},
		{command: 'Cadetblue'	, code: '0xF7A857'}
	];
	// Set Header
	$scope.$parent.showHeader();
	$scope.$parent.clearFabs();
	$scope.isExpanded = false;
	$scope.$parent.setExpanded(true);
	$scope.$parent.setHeaderFab(false);
	// Set Ink
	ionicMaterialInk.displayEffect();
	// local data
	var lights_flag;
	var applns_flag;
	scenepage.del_light = null;
	scenepage.del_appln = null;
	scenepage.light_index = null;
	$ionicTabsDelegate.select(0);
	var scenecardscroller = $ionicScrollDelegate.$getByHandle('scenecard');
	$scope.scrolldown = function () {
		scenecardscroller.scrollBottom(true);
	};
	// get data
	$scope.$watch('scenepage.notification', function (new_state, old_state) {
		if (new_state == 'add_light') {
			$ionicTabsDelegate.select(1);
		}
		else if (new_state == 'add_appln') {
			$ionicTabsDelegate.select(2);
		}
		else if (new_state == 'delete_scene') {
			$ionicTabsDelegate.select(3);
		} else {
			$ionicTabsDelegate.select(0);
		}
	});
	$scope.$watch('scenepage.del_light', function (new_index, old_index) {
		if (old_index == null) {
			setTimeout(function () {
				del_light_flag = false;
			},200);
			del_light_flag = true;
		}
		if (new_index == false) {
		}
	});
	$scope.$watch('scenepage.del_appln', function (new_index, old_index) {
		if (old_index == null) {
			setTimeout(function () {
				del_appln_flag = false;
			},200);
			del_appln_flag = true;
		}
		if (new_index == false) {
		}
	});
	$scope.toggle_remove_light = function (param) {
		switch (typeof param) {
			case 'number':
				if (scenepage.light_index!= null && scenepage.light_index == param) {
					scenepage.notification = null;
					scenepage.light_index = null;
					if (!del_light_flag) scenepage.del_light = null;
					del_light_flag = false;
					scenepage.clicked_light = scenepage.scene.lights[param];
				} else {
					scenepage.notification = 'remove_light';
					scenepage.light_index = param;
					scenepage.clicked_light = scenepage.scene.lights[param];
				};
				break;
			case 'boolean':
				if(scenepage.clicked_light && param) {
					SceneService.removeLight(scenepage.scene.id, scenepage.clicked_light.id).then(function (res) {
						refresh();
					}, function (res) {
					});
				} else {
					scenepage.del_light = null;
					scenepage.light_index = null;
					scenepage.notification = null;
				}
				delete scenepage.clicked_light;
				scenepage.notification = null;
				break;
		};
	};
	$scope.toggle_remove_appln = function (param) {
		switch (typeof param) {
			case 'number':
				if (scenepage.appln_index!= null && scenepage.appln_index == param) {
					scenepage.notification = null;
					scenepage.appln_index = null;
					if (!del_appln_flag) scenepage.del_appln = null;
					del_light_flag = false;
					scenepage.clicked_appln = scenepage.scene.appliances[param];
				} else {
					scenepage.notification = 'remove_appln';
					scenepage.appln_index = param;
					scenepage.clicked_appln = scenepage.scene.appliances[param];
				};
				break;
			case 'boolean':
				if(scenepage.clicked_appln && param) {
					SceneService.removeAppliance(scenepage.scene.id, scenepage.clicked_appln.id).then(function (res) {
						refresh();
					}, function (res) {
					});
				} else {
					scenepage.del_appln = null;
					scenepage.appln_index = null;
					scenepage.notification = null;
				}
				delete scenepage.clicked_appln;
				scenepage.notification = null;
				break;
		};
	};
	$scope.toggle_add_light = function (param) {
		if (typeof param === 'undefined') {
			SceneService.getFreeLights(scenepage.scene.id).then(function (freelights) {
				scenepage.freelights = freelights;
				scenepage.notification = (scenepage.notification==='add_light') ? (null):('add_light');
				scenepage.new_light = null;
			}, function (res) {
			});
		} else if (typeof param === 'boolean') {
			scenepage.notification = null;
			if (param == true && scenepage.new_light) {
				SceneService.addLight(scenepage.scene.id, scenepage.new_light.id, scenepage.new_light.state, scenepage.new_light.value).then(function (res) {
					refresh();
					delete scenepage.new_light;
				}, function (res) {
				});
			};
		} else {
			// TODO: error handling
		};
	};
	$scope.toggle_add_appln = function (param) {
		scenepage.notification = (scenepage.notification == 'add_appln') ? (null) : ('add_appln');
		if (param == true) {
			SceneService.addAppliance(scenepage.scene.id, scenepage.new_appln.id, scenepage.new_appln.cmdobj.command, scenepage.new_appln.cmdobj.code).then(function (res) {
				refresh();
				delete scenepage.new_appln;
			}, function (res) {
			});
		};
	};
	$scope.toggle_delete_scene = function (param) {
		scenepage.notification = (scenepage.notification == 'delete_scene') ? (null) : ('delete_scene');
		if (typeof param === 'boolean') {
			if (param) {
				SceneService.deleteScene($stateParams.index).then(function (res) {
					$rootScope.$broadcast('scene.refresh');
					$ionicHistory.goBack();
				}, function (res) {
				});
			} else {
				scenepage.notification = null;
			}
		};
	};
	var refresh = function () {
		SceneService.init().then(function (scenes) {
			scenepage.scene = SceneService.all[$stateParams.index];
			console.log(scenepage.scene.lights);
			scenepage.scene.name = scenepage.scene.name.capitalize();
			scenepage.notification = null;
			scenepage.new_light = null;
			lights_flag = false;
			applns_flag = false
			scenepage.del_light = null;
			scenepage.del_appln = null;
			scenepage.light_index = null;
			SceneService.getAllAppliances().then(function (res) {
				scenepage.appliances = res;
			}, function (error) {
			});
		}, function (error) {
		});
	};
	refresh();
});
