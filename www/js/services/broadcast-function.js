services.factory('BroadcastService', function($rootScope) {
	return function(msg, data) {
		console.log('[Broadcast]: ', msg);
		$rootScope.$broadcast(msg, data);
	}
});