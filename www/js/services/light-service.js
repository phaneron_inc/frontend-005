services.service('LightService', function($q, $http, $rootScope, BroadcastService) {
	lightService = this;
	lightService.lights = [];
	lightService.rooms = [];
	lightService.init = function () {
		var defer = $q.defer();
		$rootScope.socket.get('/lights/task', function (lights, jwres) {
			var result = updateLights(lights, jwres);
			if (result == true) {
				$rootScope.socket.post('/subscribe',  { model: 'light' });
				BroadcastService('lights');
				defer.resolve(true);
			}
			if (typeof result.code !== 'undefined') defer.reject(result.msg);
		});
		$rootScope.socket.get('/rooms', function (rooms, jwres) {
			var result = updateRooms(rooms, jwres);
			if (result == true) {
				$rootScope.socket.post('/subscribe',  { model: 'room' });
				BroadcastService('rooms');
				defer.resolve(true);
			}
			if (typeof result.code !== 'undefined') defer.reject(result.msg);
		});
		return defer.promise;
	};
	lightService.update = function (id) {
		var defer = $q.defer();
		$rootScope.socket.get('/light/?id='+id, function (light, jwres) {
			updateLight(light.id, light);
			BroadcastService('light', light.id);
		});
		return defer.promise;
	};
	lightService.getRooms = function () {
		return lightService.rooms;
	};
	lightService.getLights = function () {
		return lightService.lights;
	};
	lightService.getLight = function (id) {
		return _.find(lightService.lights, function (light) {
			return light.id == id;
		});
	};
	lightService.allOn = function () {
		var server_url = 'http://'+$rootScope.servers.remote.ip+':'+$rootScope.servers.remote.port;
		console.log(server_url);
		$http.put(server_url+'/lights/on', {withCredentials: true}, function (lights, jwres) {
			console.log('this should happen only once - on');
			var result = updateLights(lights, jwres);
			if (result == true) BroadcastService('lights-toggle');
			else console.log(result);
		});
		lightService.update();
	};
	lightService.allOff = function () {
		var server_url = 'http://'+$rootScope.servers.remote.ip+':'+$rootScope.servers.remote.port;
		console.log(server_url);
		$http.put(server_url+'/lights/off', {withCredentials: true}, function (lights, jwres) {
			console.log('this should happen only once - off');
			var result = updateLights(lights, jwres);
			if (result == true) BroadcastService('lights-toggle');
			else console.log(result);
		});
		lightService.update();
	};
	lightService.toggle = function (id) {
		$rootScope.socket.put('/light/toggle', {id: id}, function (light, jwres) {
			updateLight(id, {state: light.state});
		});
	};
	lightService.switchOn = function (id) {
		$rootScope.socket.put('/light/on', {id: id}, function (light, jwres) {
			updateLight(id, {state: light.state});
		});
	};
	lightService.switchOff = function (id) {
		$rootScope.socket.put('/light/off', {id: id}, function (light, jwres) {
			updateLight(id, {state: light.state});
		});
	};
	lightService.updateName = function (id, name) {
		$rootScope.socket.put('/light', {id: id, name: name}, function (light, jwres) {
			updateLight(id, {name: light.name});
		});
	};
	lightService.getTask = function (id) {
		var defer = $q.defer();
		$rootScope.socket.get('/light/task', {light: id}, function (taskid, jwres) {
			if (parseInt(jwres.statusCode/100) == 4 || parseInt(jwres.statusCode/100) == 5) {
				defer.reject({code: jwres.statusCode, msg: JSON.toString(jwres.error)});
			} else {
				defer.resolve(taskid[0]);
				// updateLight(id, {taskid: taskid});
			}
		});
		return defer.promise;
	};
	lightService.setTask = function (id, taskid) {
		console.log('lightid:', id);
		console.log('taskid:', taskid);
		var defer = $q.defer();
		$rootScope.socket.post('/task/light', {id: taskid, light:id}, function (task, jwres) {
			if (parseInt(jwres.statusCode/100) == 4 || parseInt(jwres.statusCode/100) == 5) {
				defer.reject({code: jwres.statusCode, msg: JSON.toString(jwres.error)});
			} else {
				defer.resolve(task.id);
			}
		});
		return defer.promise;
	};
	lightService.unsetTask = function (id, taskid) {
		console.log('lightid:', id);
		console.log('taskid:', taskid);
		var defer = $q.defer();
		$rootScope.socket.delete('/task/light', {id: taskid, light:id}, function (task, jwres) {
			if (parseInt(jwres.statusCode/100) == 4 || parseInt(jwres.statusCode/100) == 5) {
				defer.reject({code: jwres.statusCode, msg: JSON.toString(jwres.error)});
			} else {
				defer.resolve(task.id);
			}
		});
		return defer.promise;
	};
	var updateRooms = function (rooms, jwres) {
		if (parseInt(jwres.statusCode/100) == 4) return {code: jwres.statusCode, msg: 'resource not found'};
		else if (parseInt(jwres.statusCode/100) == 5) return {code: jwres.statusCode, msg: JSON.toString(jwres.error)};
		else {
			lightService.rooms = rooms;
			return true;
		}
	};
	var updateLights = function (lights, jwres) {
		if (parseInt(jwres.statusCode/100) == 4) return {code: jwres.statusCode, msg: 'resource not found'};
		else if (parseInt(jwres.statusCode/100) == 5) return {code: jwres.statusCode, msg: JSON.toString(jwres.error)};
		else {
			lightService.lights = lights;
			return true;
		}
	};
	var updateLight = function (id, options) {
		console.log(id);
		lightService.lights.map(function (light) {
			if (light.id === id) {
				for (prop in options) {
					light[prop] = options[prop];
				}
			}
			return light;
		});
		BroadcastService('light', id);
	}
	return lightService;
});