services.factory('SceneService', function ($q, $http, $rootScope) {
	sceneService = this;
	sceneService.list = [];
	sceneService.all = [];
	sceneService.light_cache = [];
	sceneService.appliances = [];
	sceneService.init = function () {
		var defer = $q.defer();
		$rootScope.socket.get('/scenes', function (res) {
			sceneService.list = res;
			$rootScope.socket.get('/scene/children', function (res) {
				console.log(res);
				sceneService.all = res;
				defer.resolve({list:sceneService.list, all:sceneService.all});
			}, function (data) {
				defer.reject(data);
			});
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.publish = function() {
		$rootScope.$broadcast('scene');
	};
	sceneService.create = function (name) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/create?name='+name, function (res) {
			sceneService.all.push(res.data);
			sceneService.publish();
			defer.resolve(true);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.getFreeLights = function (id) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/getFreeLights/'+id, function (res) {
			defer.resolve(res.data);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	}
	sceneService.update = function (id, params) {
		var defer = $q.defer();
		$rootScope.socket.put('/scene/'+id,params, function (res) {
			defer.resolve(res);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.switchon = function (index) {
		var defer = $q.defer();
		$rootScope.socket.put('/scene/set',{
			id: sceneService.list[index].id
		}, function (res) {
			sceneService.all[index].state = sceneService.all[index].state;
			sceneService.list[index].state = sceneService.list[index].state;
			sceneService.publish();
			defer.resolve();
		}, function (res) {
			defer.reject(res.data);
		});
		return defer.promise;
	};
	sceneService.getCache = function () {
		var defer = $q.defer();
		if (sceneService.light_cache.length != 0) {
			defer.resolve(sceneService.light_cache);
			sceneService.light_cache = [];
		} else {
			defer.reject('no cache');
		};
		return defer.promise;
	}
	sceneService.getlights = function (id) {
		var defer = $q.defer();
		var query = (id != null) ? ('/scene?id='+id):('/scenes/lights');
		$rootScope.socket.get(query).then(function (res) {
			sceneService.all = res.data;
			defer.resolve(sceneService.all);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.addLight = function (id, light, state, value) {
		var defer = $q.defer();
		$rootScope.socket.post('/scene/light',{
			id: id,
			light: light,
			state: state,
			value: value
		}).then(function (res) {
			$rootScope.socket.get('/scene', function (res) {
				sceneService.list = res.data;
				$rootScope.socket.get('/scene/children', function (res) {
					sceneService.all = res.data;
					defer.resolve(true);
				}, function (data) {
					defer.reject(data);
				});
			}, function (data) {
				defer.reject(data);
			});
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.removeLight = function (id, light) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/removeLight/'+id+'?id='+id+'&light='+light).then(function (res) {
			$rootScope.socket.get('/scene/getLights?id='+id).then(function (res) {
				sceneService.all = res.data;
				sceneService.light_cache = res.data[0].lights;
				sceneService.publish();
				defer.resolve(res.data[0].lights);
			});
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.addAppliance = function (id, appln, command, code) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/addAppliance/'+id+'?id='+id+'&appliance='+appln+'&command='+command+'&code='+code).then(function (res) {
			$rootScope.socket.get('/scene', function (res) {
				sceneService.list = res.data;
				$rootScope.socket.get('/scene/getEverything', function (res) {
					sceneService.all = res.data;
					defer.resolve(true);
				}, function (data) {
					defer.reject(data);
				});
			}, function (data) {
				defer.reject(data);
			});
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.removeAppliance = function (id, appln) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/removeAppliance/'+id+'?id='+id+'&appliance='+appln).then(function (res) {
			$rootScope.socket.get('/scene/getEverything?id='+id).then(function (res) {
				sceneService.all = res.data;
				sceneService.light_cache = res.data[0].lights;
				sceneService.publish();
				defer.resolve(res.data[0].lights);
			});
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.deleteScene = function (index) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/destroy/'+sceneService.list[index].id+'?id='+sceneService.list[index].id).then(function (res) {
			sceneService.list.splice(index,1);
			sceneService.all.splice(index,1);
			defer.resolve(res);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.getAllAppliances = function () {
		var defer = $q.defer();
		$rootScope.socket.get('/appliance', function (res) {
			sceneService.appln = res.data;
			defer.resolve(sceneService.appln);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	sceneService.getAppliances = function (index) {
		var defer = $q.defer();
		$rootScope.socket.get('/scene/getAppliances?id='+sceneService.list[index].id).then(function (res) {
			sceneService.appliances = res.data[0];
			defer.resolve(res.data[0]);
		}, function (data) {
			defer.reject(data);
		});
		return defer.promise;
	};
	return sceneService;
});
