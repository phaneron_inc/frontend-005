// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var services = angular.module('Phaneron.services', []);
var controllers = angular.module('Phaneron.controllers', []);
var directives = angular.module('Phaneron.directives', []);
var phaneron = angular.module('Phaneron', ['ionic', 'ionic-material', 'Phaneron.controllers' ,'Phaneron.services', 'Phaneron.directives', 'ionic-timepicker']);

phaneron.run(function ($ionicPlatform, ConnectionService) {
	$ionicPlatform.ready(function () {
		if(window.cordova && window.cordova.plugins.Keyboard) {
			cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
			cordova.plugins.Keyboard.disableScroll(true);
		}
		if(window.StatusBar) {
			StatusBar.styleDefault();
		}
		ConnectionService.init();
	});
});

phaneron.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
	.state('app', {
		url: '/app',
		abstract: true,
		templateUrl: 'templates/main.html'
	})
	.state('app.lights', {
		url: '/lights',
		views: {
			'menuContent': {
				templateUrl: 'templates/lights.html'
			}
		}
	})
	.state('app.light', {
		url: '/light/:id',
		views: {
			'menuContent': {
				templateUrl: 'templates/light.html'
			}
		}
	})
	.state('app.schedules', {
		url: '/schedules',
		views: {
			'menuContent': {
				templateUrl: 'templates/schedules.html'
			}
		}
	})
	.state('app.schedule', {
		url: '/schedule/:id',
		views: {
			'menuContent': {
				templateUrl: 'templates/schedule.html'
			}
		}
	})
	.state('app.settings', {
		url: '/settings',
		views: {
			'menuContent': {
				templateUrl: 'templates/settings.html'
			}
		}
	})
	.state('app.feedback', {
		url: '/feedback',
		views: {
			'menuContent': {
				templateUrl: 'templates/feedback.html'
			}
		}
	});
	$urlRouterProvider.otherwise('/app/lights');
});