'use strict';

directives.directive('ngLastRepeat', function () {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			scope.toprint = 'ngLastRepeat'+ (attr.ngLastRepeat ? '.'+attr.ngLastRepeat : '');
			if (scope.$last === true) {
				scope.$emit(scope.toprint);
			}
		}
	}
});
directives.directive('phWeekday', function () {
	return {
		restrict: 'E',
		scope: {
			days: '=ngModel',
			onChange: '=ngChange',
			isActive: '=active',
			lastIndex: '=phLast'
		},
		controller: function ($scope) {
			
		},
		link: function (scope, element, attr) {
			scope.labels = ['Sunday','Monday','Tueday','Wednesday','Thursday','Friday','Saturday'];
			scope.miniLabels = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
			scope.microLabels = ['S','M','T','W','T','F','S'];
			scope.toggleDay = function (index) {
				if (_.countBy(scope.days)['true'] != 1 || scope.days[index] == false) {
					scope.days[index] = !scope.days[index];
				}
				scope.onChange();
				// setTimeout(function () {
				// },100);
			};
		},
		templateUrl:'templates/weekday-directive.html'
	}
});
